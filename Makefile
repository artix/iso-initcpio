SYSCONFDIR = /etc
CPIODIR = $(SYSCONFDIR)/initcpio

DIRMODE = -dm0755
MODE =  -m0644
XMODE = -m0755

CONFIG = \
	$(wildcard config/*.conf)

CPIOHOOKS = \
	$(wildcard hooks/*)

CPIOINST = \
	$(wildcard install/*)

SCRIPT = \
	$(wildcard script/*)

install_initcpio:
	install $(DIRMODE) $(DESTDIR)$(CPIODIR)/hooks
	install $(MODE) $(CPIOHOOKS) $(DESTDIR)$(CPIODIR)/hooks

	install $(DIRMODE) $(DESTDIR)$(CPIODIR)/install
	install $(MODE) $(CPIOINST) $(DESTDIR)$(CPIODIR)/install
	install $(XMODE) $(SCRIPT) $(DESTDIR)$(CPIODIR)

	install $(MODE) $(CONFIG) $(DESTDIR)$(SYSCONFDIR)

install: install_initcpio

.PHONY: install
